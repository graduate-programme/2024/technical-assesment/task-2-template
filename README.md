# Task 2: Django CRUD API

This is simple template to help you get started with task 2.

## Using this template

### Step 1: Create the virtual environment and install requirements

```bash
pyhton3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

### Step 2: Start the project

#### Docker

> **_NOTE:_**  Docker and docker compose must be installed on the host machine. Follow [these](https://docs.docker.com/engine/install/) steps to install.

To run the container:
```bash
docker compose up -d --build
```

- ```-d``` flag runs the container in a deamon [suggested]
- ```--build``` rebuilds the dokcer image [optional]

Stopping the container:
```bash
docker compose down --rmi local
```
- ```--rmi local``` removes local docker images [optional]

#### Locally

From the project root:
```bash
python3 src/manage.py runserver 8000
```

> Setting the python path to the src directory is required for tests rooted in project root.


## Steps to create this template

### Step 1: Create the virtual environment and install requirements

```bash
pyhton3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

### Step 2: Create the django project

```bash
mkdir src
django-admin startproject albums src
```

Positional arguments:
- ```albums``` Name of project
- ```src``` Destination directory
The name can be anything - 'albums' is a suggestion.
The destination directory can be ```.```. In this case, Django will create the project inside a folder matching the project name.

Esure your localhost is added to ```ALLOWED_HOSTS``` in ```settings.py```. Read more about allowed hosts [here](https://docs.djangoproject.com/en/4.2/ref/settings/#allowed-hosts).

### Step 3: Set up docker

> **_NOTE:_**  Docker and docker compose must be installed on the host machine. Follow [these](https://docs.docker.com/engine/install/) steps to install.

The ```Dockerfile``` copies the required files, and installs the requirements inside the docker container.

Docker compose is used to easily start the container.

To run the container:
```bash
docker compose up -d --build
```

- ```-d``` flag runs the container in a deamon [suggested]
- ```--build``` rebuilds the dokcer image [optional]

Stopping the container:
```bash
docker compose down --rmi local
```
- ```--rmi local``` removes local docker images [optional]
